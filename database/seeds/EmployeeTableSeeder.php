<?php

use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['id', 'first_name', 'last_name', 'id_number'];
        $values = [
            [1, 'Arne', 'Asbjørnsen', '12345'],
            [2, 'Bjørnar', 'Bergh', '24680'],
            [3, 'Catharina', 'Capteyn', '36912'],
            [4, 'Dieter', 'Dürr', '48126'],
            [5, 'Ebubekir', 'Ekmekçi', '51015'],
            [6, 'Františka', 'Fojtik', '61218'],
        ];

        // ‘deep-combine’ array keys and values
        $data = array_map(function ($row) use ($keys) {
            return array_combine($keys, $row);
        }, $values);

        DB::table('employees')->insert($data);
    }
}
