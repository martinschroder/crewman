<?php

use Illuminate\Database\Seeder;

class AssignmentEmploymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['assignment_id', 'employment_id'];
        $values = [
            [1, 6],
            [2, 6],
            [3, 11],
            [4, 11],
            [5, 11],
            [6, 7],
            [6, 2],
            [10, 1],
            [10, 8],
            [11, 1],
            [11, 8],
            [12, 8],
            [13, 9],
            [14, 9],
            [15, 9],
            [16, 3],
        ];

        // ‘deep-combine’ array keys and values
        $data = array_map(function ($row) use ($keys) {
            return array_combine($keys, $row);
        }, $values);

        DB::table('assignment_employment')->insert($data);
    }
}
