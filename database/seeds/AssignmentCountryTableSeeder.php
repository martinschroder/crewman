<?php

use Illuminate\Database\Seeder;

class AssignmentCountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['assignment_id', 'country_id'];
        $values = [
            [1, 1],
            [1, 3],
            [2, 2],
            [3, 1],
            [4, 1],
            [4, 3],
            [5, 3],
            [6, 1],
            [7, 1],
            [8, 1],
            [9, 1],
            [10, 1],
            [11, 1],
            [12, 1],
            [13, 1],
        ];

        // ‘deep-combine’ array keys and values
        $data = array_map(function ($row) use ($keys) {
            return array_combine($keys, $row);
        }, $values);

        DB::table('assignment_country')->insert($data);
    }
}
