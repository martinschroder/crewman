<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['assignment_id', 'employee_id', 'name', 'type', 'start_date', 'end_date'];
        $values = [
            [1, 3, 'Pilot', 'position', '2019-08-01 07:00:00', '2019-08-01 15:30:00'],
            [2, 3, 'Student', 'qualification', '2019-10-02 08:00:00', '2019-10-04 13:00:00'],
            [3, 6, 'Contact person', 'position', '2019-08-01 06:00:00', '2019-08-01 07:00:00'],
            [4, 6, 'Crew leader', 'position', '2019-08-01 07:00:00', '2019-08-01 15:30:00'],
            [5, 6, 'Contact person', 'position', '2019-08-01 15:30:00', '2019-08-01 17:00:00'],
            [6, 1, 'X-ray screen controller', 'position', '2019-07-30 12:00:00', '2019-07-30 15:00:00'],
            [6, 1, 'Body examiner', 'position', '2019-08-01 05:00:00', '2019-08-01 09:00:00'],
            [6, 1, 'Metal detector controller', 'position', '2019-08-03 10:00:00', '2019-08-03 14:00'],
            [7, 2, 'Hydraulics revisor', 'position', '2019-07-31 20:00:00', '2019-08-01 00:00:00'],
            [8, 2, 'Electrician', 'position', '2019-08-31 20:00:00', '2019-09-01 00:00:00'],
            [9, 2, 'Shift leader', 'position', '2019-09-30 20:00:00', '2019-10-01 00:00:00'],
            [12, 4, 'Main responsible', 'position', '2019-07-21 13:35:00', '2019-07-21 13:37:00'],
            [13, 5, 'Salad cutter', 'position', '2019-08-01 06:00:00', '2019-08-01 07:00:00'],
        ];

        // ‘deep-combine’ array keys and values
        $data = array_map(function ($row) use ($keys) {
            return array_combine($keys, $row);
        }, $values);

        DB::table('roles')->insert($data);
    }
}
