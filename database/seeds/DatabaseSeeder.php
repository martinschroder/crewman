<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EmployeeTableSeeder::class,
            EmploymentTableSeeder::class,
            EmployerTableSeeder::class,
            AssignmentTableSeeder::class,
            CountryTableSeeder::class,
            AssignmentCountryTableSeeder::class,
            RolesTableSeeder::class,
            LeavesTableSeeder::class,
            AssignmentEmploymentTableSeeder::class,
        ]);
    }
}
