<?php

use Illuminate\Database\Seeder;

class EmployerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'name' => 'Trøndelag Air'],
            ['id' => 2, 'name' => 'Namnam Catering'],
            ['id' => 3, 'name' => 'Juling Security Staff'],
            ['id' => 4, 'name' => 'Flekk Vekk!'],
            ['id' => 5, 'name' => 'Andersen snekkeri og maskinvedlikehold'],
            ['id' => 6, 'name' => 'Det nationale flysikkerhetstilsynet'],
        ];

        DB::table('employers')->insert($data);
    }
}
