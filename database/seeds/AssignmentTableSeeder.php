<?php

use Illuminate\Database\Seeder;

class AssignmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['employer_id', 'description', 'start_date', 'end_date'];
        $values = [
            [1, 'Fly from Trondheim to Kathmandu', '2019-08-01 07:00:00', '2019-08-01 15:30:00'],
            [6, 'Workshop: How to handle the Boeing 737 Max', '2019-10-02 08:00:00', '2019-10-04 13:00'],
            [1, 'Welcome passengers', '2019-08-01 06:00:00', '2019-08-01 07:00:00'],
            [1, 'Attend the Katmandu flight', '2019-08-01 07:00:00', '2019-08-01 15:30:00'],
            [1, 'Inform passengers about Nepal', '2019-08-01 15:30:00', '2019-08-01 17:00:00'],
            [3, 'Staff the security checkpoint', '2016-01-01 00:00:00', null],
            [5, 'Monthly routine check', '2019-07-31 20:00:00', '2019-08-01 00:00:00'],
            [5, 'Monthly routine check', '2019-08-31 20:00:00', '2019-09-01 00:00:00'],
            [5, 'Monthly routine check', '2019-09-30 20:00:00', '2019-10-01 00:00:00'],
            [4, 'Clean the departure hall', '2016-01-01 00:00:00', null],
            [4, 'Clean the toilets', '2016-01-01 00:00:00', null],
            [4, 'Clean the big coffee stain near baggage belt 3', '2019-07-21 13:35:00', '2019-07-21 13:37:00'],
            [2, 'Make sandwiches', '2019-07-01 06:00:00', '2019-07-01 07:00:00'],
            [2, 'Make pancakes', '2019-08-01 06:00:00', '2019-08-01 07:00:00'],
            [2, 'Make hot dogs', '2019-09-01 06:00:00', '2019-09-01 07:00:00'],
            [1, 'Paint a plain', '2017-03-01 00:00:00', '2017-04-01 00:00:00'],
        ];

        // ‘deep-combine’ array keys and values
        $data = array_map(function ($row) use ($keys) {
            return array_combine($keys, $row);
        }, $values);

        DB::table('assignments')->insert($data);
    }
}
