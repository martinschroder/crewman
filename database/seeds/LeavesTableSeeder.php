<?php

use Illuminate\Database\Seeder;

class LeavesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['assignment_id', 'employee_id', 'reason', 'start_date', 'end_date'];
        $values = [
            [2, 3, 'Food poisoning', '2019-10-03 15:00:00', '2019-10-03 21:00:00'],
        ];

        // ‘deep-combine’ array keys and values
        $data = array_map(function ($row) use ($keys) {
            return array_combine($keys, $row);
        }, $values);

        DB::table('leaves')->insert($data);
    }
}
