<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'name' => 'Norway'],
            ['id' => 2, 'name' => 'Sweden'],
            ['id' => 3, 'name' => 'Nepal'],
        ];

        DB::table('countries')->insert($data);
    }
}
