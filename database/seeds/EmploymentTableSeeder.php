<?php

use Illuminate\Database\Seeder;

class EmploymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['employee_id', 'name', 'start_date', 'end_date'];
        $values = [
            [1, 'Cleaner', '2016-01-01', '2016-12-31'],
            [1, 'Security staff', '2018-01-01', null],
            [2, 'Technician', '2016-01-01', '2018-02-28'],
            [2, 'Technician', '2020-01-01', null],
            [3, 'Flight attendant', '2016-01-01', '2017-12-31'],
            [3, 'Pilot', '2018-01-01', null],
            [4, 'Security staff', '2016-01-01', '2018-12-31'],
            [4, 'Cleaner', '2019-01-01', null],
            [5, 'Cook', '2019-01-01', null],
            [6, 'Cook', '2017-01-01', '2017-06-30'],
            [6, 'Flight attendant', '2017-07-01', null],
        ];

        // ‘deep-combine’ array keys and values
        $data = array_map(function ($row) use ($keys) {
            return array_combine($keys, $row);
        }, $values);

        DB::table('employments')->insert($data);
    }
}
