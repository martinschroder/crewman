/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/script.js":
/*!********************************!*\
  !*** ./resources/js/script.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(function () {
  $('[data-toggle="popover"]').popover();
  $('[data-toggle="tooltip"]').tooltip();
  $('#employees-table').DataTable({
    'info': false,
    'paging': false,
    'responsive': true,
    'order': [[1, 'asc']],
    'initComplete': function initComplete(settings, json) {
      $('#employees-table_wrapper').toggleClass('d-none', $('#employees-table').hasClass('d-none'));
      $('#employees-table').removeClass('d-none');
    }
  });
});
$(function () {
  var arrow_left = $('header:not(.small) svg #arrow').position().left + 8;
  var arrow_top = $('header:not(.small) svg #arrow').position().top + 12;
  var scroll_start_left = $(window).scrollLeft();
  var scroll_start_top = $(window).scrollTop();
  /**
   * Animate the arrow in the OSM logo.
   */

  $(document).mousemove(function (event) {
    var diff_x = event.pageX - arrow_left + scroll_start_left - $(window).scrollLeft();
    var diff_y = event.pageY - arrow_top + scroll_start_top - $(window).scrollTop();
    var angle = 90 + Math.atan2(diff_y, diff_x) * 180 / Math.PI;
    $('header:not(.small) svg #arrow').attr('transform', 'rotate(' + angle + ', 56.83, 34.5)');
  });
  /**
   * Follow the ‘link’ (`data-href` attribute) on elements with the class `.clickable`.
   */

  $('.clickable').click(function () {
    var href = $(this).data('href');

    if (typeof href !== 'undefined' && href != '') {
      window.location.href = href;
    }
  });
  /**
   * Shrink and unshrink the header when scrolling.
   */

  $(document).scroll(function (event) {
    if (document.body.scrollTop > 15 || document.documentElement.scrollTop > 15) {
      $('header').addClass('small');
    } else {
      $('header').removeClass('small');
    }
  });
  /**
   * Toggle grid and list display on employee overview.
   */

  $('#employees-display-selector a').click(function (event) {
    event.preventDefault();
    var selector = $(event.target).closest('a');

    if (selector.hasClass('disabled')) {
      return;
    }

    selector.tooltip('hide');
    $('#employees-display-selector a').toggleClass('disabled');
    $('#employees-grid, #employees-table_wrapper').toggleClass('d-none');
    window.history.pushState('', '', selector.attr('href'));
  });
});

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!****************************************************************!*\
  !*** multi ./resources/js/script.js ./resources/sass/app.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/vagrant/Crewman/resources/js/script.js */"./resources/js/script.js");
module.exports = __webpack_require__(/*! /home/vagrant/Crewman/resources/sass/app.scss */"./resources/sass/app.scss");


/***/ })

/******/ });