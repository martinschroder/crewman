<?php

namespace Crewman;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    public function employer()
    {
        return $this->belongsTo('Crewman\Employer');
    }

    public function getPeriodAttribute()
    {
        $start_date_day = date('Y-m-d', strtotime($this->start_date));

        if (is_null($this->end_date)) {
            $period = sprintf('Since %s', $start_date_day);
        } else {
            $end_date_day = date('Y-m-d', strtotime($this->end_date));
            $period = $start_date_day == $end_date_day
                ? $start_date_day
                : sprintf('%s–%s', $start_date_day, $end_date_day);
        }

        return $period;
    }
}
