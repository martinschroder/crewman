<?php

namespace Crewman;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    public function employments()
    {
        return $this->hasMany('Crewman\Employment');
    }

    public function roles()
    {
        return $this->hasMany('Crewman\Role');
    }

    public function leaves()
    {
        return $this->hasMany('Crewman\Leave');
    }

    public function getMostRecentEmployment()
    {
        $today = date('Y-m-d');

        return $this->employments()
            ->where('start_date', '<=', $today)  // no future employments
            ->orderBy('start_date', 'desc')
            ->first()
            ?? new Employment;
    }

    public function getMostRecentEmployer()
    {
        return $this->getMostRecentEmployment()->getMostRecentAssignment()->employer;
    }

    public function getRoleInAssignment($assignment_id)
    {
        return $this->roles->where('assignment_id', $assignment_id)->first() ?? new Role;
    }

    public function getLeavesInAssignment($assignment_id)
    {
        return $this->leaves->where('assignment_id', $assignment_id) ?? [];
    }

    public function getFullNameAttribute()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }
}
