<?php

namespace Crewman;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    public function assignments()
    {
        return $this->belongsToMany('Crewman\Assignment');
    }

    public function getMostRecentAssignment()
    {
        $now = date('Y-m-d H:i:s');

        return $this->assignments()
            ->where('start_date', '<=', $now)  // no future assignments
            ->orderBy('start_date', 'desc')
            ->first()
            ?? new Assignment;
    }

    public function isCurrent()
    {
        $now = date('Y-m-d');
        return $this->start_date <= $now && (is_null($this->end_date) || $this->end_date > $now);
    }

    public function isPast()
    {
        return $this->end_date <= date('Y-m-d');
    }

    public function isFuture()
    {
        return $this->start_date > date('Y-m-d');
    }
}
