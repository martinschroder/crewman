$(function () {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
    $('#employees-table').DataTable({
        'info': false,
        'paging': false,
        'responsive': true,
        'order': [[1, 'asc']],
        'initComplete': function (settings, json) {
            $('#employees-table_wrapper').toggleClass('d-none', $('#employees-table').hasClass('d-none'));
            $('#employees-table').removeClass('d-none');
        }
    });
});

$(function() {
    let arrow_left = $('header:not(.small) svg #arrow').position().left + 8;
    let arrow_top = $('header:not(.small) svg #arrow').position().top + 12;
    let scroll_start_left = $(window).scrollLeft();
    let scroll_start_top = $(window).scrollTop();

    /**
     * Animate the arrow in the OSM logo.
     */
    $(document).mousemove(function (event) {
        let diff_x = event.pageX - arrow_left + scroll_start_left - $(window).scrollLeft();
        let diff_y = event.pageY - arrow_top + scroll_start_top - $(window).scrollTop();
        let angle = 90 + Math.atan2(diff_y, diff_x) * 180 / Math.PI;

        $('header:not(.small) svg #arrow').attr('transform', 'rotate(' + angle + ', 56.83, 34.5)');
    });

    /**
     * Follow the ‘link’ (`data-href` attribute) on elements with the class `.clickable`.
     */
    $('.clickable').click(function () {
        let href = $(this).data('href');
        if (typeof href !== 'undefined' && href != '') {
            window.location.href = href;
        }
    });

    /**
     * Shrink and unshrink the header when scrolling.
     */
    $(document).scroll(function (event) {
        if (document.body.scrollTop > 15 || document.documentElement.scrollTop > 15) {
            $('header').addClass('small');
        } else {
            $('header').removeClass('small');
        }
    });

    /**
     * Toggle grid and list display on employee overview.
     */
    $('#employees-display-selector a').click(function (event) {
        event.preventDefault();
        let selector = $(event.target).closest('a');

        if (selector.hasClass('disabled')) {
            return;
        }

        selector.tooltip('hide');
        $('#employees-display-selector a').toggleClass('disabled');
        $('#employees-grid, #employees-table_wrapper').toggleClass('d-none');
        window.history.pushState('', '', selector.attr('href'));
    });
});
