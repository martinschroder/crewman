@extends('layouts.main')
@section('title', 'CREWMAN – Employee details: ' . $employee->full_name)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div id="abstract" class="sticky-top">
                    <div class="profile-picture">
                        <img src="{{ asset('images/profile_pictures/' . $employee->id_number . '.jpg') }}" alt="Profile picture" />
                    </div>

                    <div>
                        <strong>{{ $employee->full_name }}</strong>
                    </div>

                    <div>
                        <strong>ID number: </strong>{{ $employee->id_number }}
                    </div>

                    <div id="quick-actions" class="list-group">
                        @foreach ([
                            ['action' => null, 'text' => 'Messages', 'badge' => ['text' => '2', 'type' => 'info']],
                            ['action' => null, 'text' => 'Calendar', 'badge' => null],
                            ['action' => null, 'text' => 'Contract details', 'badge' => ['text' => 'Expiring', 'type' => 'danger']],
                        ] as $row)
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" href="{{ $row['action'] }}">
                                {{ $row['text'] }}
                                @if (!is_null($row['badge']))
                                    <span class="badge badge-pill badge-{{ $row['badge']['type'] }}">{{ $row['badge']['text'] }}</span>
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <nav class="sticky-top">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('index', app('request')->has('display') ? ['display' => app('request')->input('display')] : null) }}">
                                Employees
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            {{ $employee->full_name }}
                        </li>
                    </ol>
                </nav>

                <div id="employments" class="accordion">
                    <h1>Employments</h1>

                    @foreach ($employee->employments->reverse() as $employment)
                        <div class="card">
                            <h2 class="clickable card-header" data-toggle="collapse" data-target="#employment-{{ $employment->id }}">
                                {{ $employment->name }}

                                <small>
                                    @isset($employment->end_date)
                                        <span class="text-nowrap">{{ $employment->start_date }}</span>–<span class="text-nowrap">{{ $employment->end_date }}</span>
                                    @elseif($employment->start_date <= date('Y-m-d'))
                                        since <span class="text-nowrap">{{ $employment->start_date }}</span>
                                    @else
                                        from <span class="text-nowrap">{{ $employment->start_date }}</span>
                                    @endif
                                </small>
                            </h2>

                            <div id="employment-{{ $employment->id }}" class="collapse{{ $employment->isCurrent() ? ' show' : '' }}" data-parent="#employments">
                                <div class="card-body">
                                    @if (count($employment->assignments) > 0)
                                        <h3>Assignments</h3>
                                        <table id="assignments" class="table table-striped table-hover">
                                            <thead>
                                                <th>Assignment</th>
                                                <th>Employer</th>
                                                <th>Role</th>
                                                <th>Period</th>
                                            </thead>

                                            <tbody>
                                                @foreach ($employment->assignments as $assignment)
                                                    @php ($role = $employee->getRoleInAssignment($assignment->id))
                                                    @php ($leaves = $employee->getLeavesInAssignment($assignment->id))

                                                    <tr data-toggle="popover" data-html="true" data-trigger="hover" title="Assignment details" data-content="@include('includes.assignment_details', ['assignment' => $assignment, 'leaves' => $leaves])">
                                                        <td>{{ $assignment->description }}</td>
                                                        <td>{{ $assignment->employer->name }}</td>
                                                        <td>
                                                            @if (!empty($role->id))
                                                                {{ $role->name }} <i>({{ ucfirst($role->type) }})</i>
                                                            @endisset
                                                        </td>
                                                        <td>
                                                            {!! preg_replace('/(\d{4}-\d{2}-\d{2})/', '<span class="text-nowrap">$1</span>', $assignment->period) !!}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        No assignments
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach

                    @if (count($employee->employments) < 2)
                        <div class="card" />
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
