@extends('layouts.main')
@section('title', 'CREWMAN – Employee overview')
@section('content')
    <div class="container">
        <div class="row">
            <h1 class="mt-4">
                Employees
                <small>{{ count($employees) }} employees found</small>
            </h1>

            <div id="employees-display-selector" class="w-100 d-flex justify-content-end">
                <a class="{{ app('request')->input('display') != 'list' ? 'disabled' : '' }}" href="{{ route('index') }}" title="Display as grid" data-toggle="tooltip">
                    <span class="fa">&#61450;</span>
                </a>
                <a class="{{ app('request')->input('display') == 'list' ? 'disabled' : '' }}" href="{{ route('index', ['display' => 'list']) }}" title="Display as list" data-toggle="tooltip">
                    <span class="fa{{ app('request')->input('display') !== 'list' ? ' highlight_color' : '' }}">&#61497;</span>
                </a>
            </div>

            <div id="employees-grid" class="card-deck{{ app('request')->input('display') == 'list' ? ' d-none' : '' }}">
                @foreach ($employees as $employee)
                    <div class="card mb-4 clickable" data-href="{{ route('show', ['id' => $employee->id]) }}">
                        <div class="card-image-container">
                            <img class="card-img-top img-fluid" src="{{ asset('images/profile_pictures/' . $employee->id_number . '.jpg') }}" alt="Profile picture" />
                        </div>

                        <div class="card-body">
                            <h5 class="card-title">
                                {{ $employee->full_name }}
                            </h5>
                            <div class="card-text">
                                <p>
                                    <div><strong>ID number</strong></div>
                                    <div>{{ $employee->id_number }}</div>
                                </p>
                                <p>
                                    <div><strong>Employer</strong></div>
                                    <div>{{ $employee->getMostRecentEmployer()->name ?? '' }}</div>
                                </p>
                            </div>
                        </div>
                    </div>

                    @if ($loop->iteration % 2 == 0)
                        {{-- line break after 2 cards for sm --}}
                        <div class="w-100 d-none d-sm-block d-md-none"></div>
                    @endif
                    @if ($loop->iteration % 3 == 0)
                        {{-- line break after 3 cards for md --}}
                        <div class="w-100 d-none d-md-block d-lg-none"></div>
                    @endif
                    @if ($loop->iteration % 4 == 0)
                        {{-- line break after 4 cards for lg --}}
                        <div class="w-100 d-none d-lg-block d-xl-none"></div>
                    @endif
                    @if ($loop->iteration % 5 == 0)
                        {{-- line break after 5 cards for xl --}}
                        <div class="w-100 d-none d-xl-block"></div>
                    @endif
                @endforeach

                @for ($i = 0; $i < 4; $i++)
                    <div class="card" style="height: 0; border: 0;"></div>
                    @if ((count($employees) + $i + 1) % 2 == 0)
                        <div class="w-100 d-none d-sm-block d-md-none"></div>
                    @endif
                    @if ((count($employees) + $i + 1) % 3 == 0)
                        <div class="w-100 d-none d-md-block d-lg-none"></div>
                    @endif
                    @if ((count($employees) + $i + 1) % 4 == 0)
                        <div class="w-100 d-none d-lg-block d-xl-none"></div>
                    @endif
                    @if ((count($employees) + $i + 1) % 5 == 0)
                        <div class="w-100 d-none d-xl-block"></div>
                    @endif
                @endfor
            </div>

            <table id="employees-table" class="table table-striped table-hover{{ app('request')->input('display') != 'list' ? ' d-none' : '' }}">
                <thead>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>ID number</th>
                    <th>Most recent employer</th>
                </thead>

                <tbody>
                    @foreach ($employees as $employee)
                        <tr class="clickable" data-href="{{ route('show', ['id' => $employee->id, 'display' => 'list']) }}">
                            <td>{{ $employee->first_name }}</td>
                            <td>{{ $employee->last_name }}</td>
                            <td>{{ $employee->id_number }}</td>
                            <td>{{ $employee->getMostRecentEmployer()->name ?? '' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
