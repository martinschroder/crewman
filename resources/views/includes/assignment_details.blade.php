<div class='d-table-row'>
    <span class='d-table-cell text-nowrap'>
        <strong>Start date:&nbsp;</strong>
    </span>
    <span class='d-table-cell'>
        {{ $assignment->start_date }}
    </span>
</div>

@if (!is_null($assignment->end_date))
    <div class='d-table-row'>
        <span class='d-table-cell'>
            <strong>End date: </strong>
        </span>
        <span class='d-table-cell'>
            {{ $assignment->end_date }}
        </span>
    </div>
@endif

@if (count($leaves) > 0)
    <div class='d-table-row'>
        <span class='d-table-cell'>
            <strong>Leave{{ count($leaves) > 1 ? 's' : '' }}: </strong>
        </span>
        <span class='d-table-cell align-top'>
            <ul>
                @foreach ($leaves as $leave)
                    <li>{{ $leave->start_date }}–{{ $leave->end_date }} ({{ $leave->reason }})</li>
                @endforeach
            </ul>
        </span>
    </div>
@endif
