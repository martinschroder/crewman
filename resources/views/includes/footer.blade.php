<footer>
    <div class="contact">
        <span>Martin Schrøder</span>
        <span><span class="fa">&#61443;</span>m.schroder@posteo.no</span>
        <span><span class="fa">&#61589;</span>+47 96 90 70 97</span>
    </div>

    <div class="copyright">
        © 2019 Martin Schrøder
    </div>
</footer>
