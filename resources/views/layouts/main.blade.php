<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
@include('includes.css')
@include('includes.js')
<title>@yield('title')</title>
</head>
<body>
    @include('includes.header')
    <main class="main">
        @yield('content')
    </main>
    @include('includes.footer')
</body>
</html>
